//Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования

//Есть спецсимволы. Если нам не нужно использовать их по назначению, а просто вывести как текст,
//то мы должны использовать экранирование - \, иначе возникнет ошибка, компьютер будет считать спецсимвол как часть программы
// наприклад ім'я, якщо схочемо вивести 'ім'я', буде помилка, треба ось так 'ім\'я'

//Какие средства объявления функций вы знаете?
//Существует три способа объявления функции: Function Declaration, Function Expression и Named Function Expression.
//напамять не вспомнила, пришлось гуглить

//Что такое hoisting как он работает для переменных и функций?
//это когда переменные и функции используют до обьявления переменной или функции
// в случае с переменной поддерживает var, let и const нет, если внутри функции, то поднимается к началу этой функции
//в случае с функцией поднимается только ее обьявление





// Task
// Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую.
// Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.
//Task
//При вызове функция должна запросить дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
//Создать метод getAge(), который будет возвращать сколько пользователю лет.
//Создать метод getPassword(), возвращающий первую букву имени пользователя в верхнем регистре,
//соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
//Вывести в консоль результат работы функции createNewUser(), а также функций getAge()и getPassword() созданного объекта.

function createNewUser (){

	newUser = {
		firstName: prompt ('Як вас звати?', 'Валентин'),
		lastName: prompt ('Ваше прізвище?', 'Потапов'),
		birthday: prompt('Треба вашу дату народження: dd.mm.yy:', '01.02.1980'),

		get newUser() {
			return `${this.firstName} ${this.lastName}`;
		 },
	  
		 set newUser(value) {
			[this.firstName, this.lastName] = value.split(" ");
		 },

	};

newUser.getPassword = function () {
	return `${this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6)}`;
 };

 newUser.getAge = function () {
		  return `${new Date().getFullYear() - newUser.birthday.slice(6)}`;
		};

return newUser
}


let user = createNewUser();
console.log(user.getPassword());
console.log("Вам " + user.getAge() + " років");


